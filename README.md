# Butterbot

It's alive!

## Configuring

### Requirements

* you will need a slack admin account for this
* you will need a host in which to run this
* you will need this host to be able to be reached on port 3000 (by default)
* you will need lots of humor and good mood

### In Slack

1. Create a new application on https://api.slack.com/apps
1. Add the image butterbot-app.jpn
1. Expand add features and functionalities
1. Select interactive components
    1. Introduce the request URL, butterbot will expect to be called in the `/interaction` path
1. Select Add a bot user
    1. Introduce butterbot as user (or whatever you like)
1. Select permissions and install in your workspace
1. Follow https://gitlab.com/pcarranza/oauth-token instructions to get a valid auth token
1. Create a `launch-butterbot` script using the captured configuration
1. Profit

## Required environment variables

- `BOT_ID` get this value during the authorization process
- `BOT_TOKEN` get this value during the authorization process
- `VERIFICATION_TOKEN` capture from the bot registration page in Slack
- `CHANNEL_ID` fill with an invalid value, capture from the log in the first execution
- `ALLOWED_USERS` the Slack usernames that are allowed to talk to the butterbot
- `OPTIONS` in key=value format split by commas
- `CMD` command to execute, will be concatenated with the selected option

## Optional environment variables

- `PORT` defines in which port butterbot listens for http requests from Slack
