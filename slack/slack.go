package slack

import (
	"fmt"
	"log"
	"strings"

	"github.com/nlopes/slack"
	"gitlab.com/gl-infra/butterbot/actions"
)

// Slack wraps the slack client among other things
type Slack struct {
	client *slack.Client
	rtm    *slack.RTM
	config Config
}

// Config represents the required configuration for managing the slack interaction
type Config interface {
	GetBotID() string
	IsValidChannel(string) bool
	GetOptions() (map[string]string, error)
	GetDebug() bool
}

// New returns a new Slack object
func New(token string, c Config) *Slack {
	client := slack.New(token)
	client.SetDebug(c.GetDebug())
	return &Slack{
		client: client,
		config: c,
	}
}

// SendMessage implements the Messager interface
func (s *Slack) SendMessage(channel, message string) error {
	return s.SendMessageWithParams(channel, message, slack.PostMessageParameters{})
}

// SendMessageWithParams provides a much more complex interface to build a message with post params
func (s *Slack) SendMessageWithParams(channel, message string, params slack.PostMessageParameters) error {
	if _, _, err := s.client.PostMessage(channel, message, params); err != nil {
		return fmt.Errorf("failed to post message: %s", err)
	}
	return nil
}

// Listen starts a eealtime tracking of slack events
func (s *Slack) Listen() {
	s.rtm = s.client.NewRTM()

	// Start listening slack events
	go s.rtm.ManageConnection()

	// Handle slack events
	for msg := range s.rtm.IncomingEvents {
		switch ev := msg.Data.(type) {
		case *slack.MessageEvent:
			if err := s.handleMessageEvent(ev); err != nil {
				log.Printf("[ERROR] Failed to handle message: %s", err)
			}
		}
	}
}

// handleMesageEvent handles slack message events.
func (s *Slack) handleMessageEvent(ev *slack.MessageEvent) error {
	// Only reply to messages in specific channel.
	if !s.config.IsValidChannel(ev.Channel) {
		log.Printf("Message '%s' on channel %s is ignored\n", ev.Msg.Text, ev.Channel)
		s.leaveChannel(ev.Channel)
		return nil
	}

	// Only reply mentions to bot.
	if !strings.HasPrefix(ev.Msg.Text, fmt.Sprintf("<@%s>", s.config.GetBotID())) {
		return nil
	}

	user, err := s.client.GetUserInfo(ev.Msg.User)
	if err != nil {
		log.Printf("Could not find user with id %s", ev.Msg.User)
	}
	// Only reply to allowed users
	if !actions.IsAllowedFor(user.Name) {
		log.Printf("Ignoring message from not allowed user %+v", user)
		return nil
	}

	// Parse message
	m := strings.Split(strings.TrimSpace(ev.Msg.Text), " ")[1:]
	if len(m) == 0 || strings.Join(m, " ") != "you restart gitaly" {
		return s.SendMessage(ev.Channel, "what is my purpose?")
	}

	// value is passed to message handler when request is approved.
	attachment := slack.Attachment{
		Text:       "which gitaly?",
		Color:      "#d1d1e0",
		CallbackID: "butterbot",
		Actions: []slack.AttachmentAction{
			{
				Name:    actions.ActionSelect,
				Type:    "select",
				Options: s.getSlackOptionsList(),
			},

			{
				Name:  actions.ActionCancel,
				Text:  "Cancel",
				Type:  "button",
				Style: "danger",
			},
		},
	}

	params := slack.PostMessageParameters{
		Attachments: []slack.Attachment{
			attachment,
		},
	}

	return s.SendMessageWithParams(ev.Channel, "", params)
}

func (s *Slack) getSlackOptionsList() []slack.AttachmentActionOption {
	optionsList := make([]slack.AttachmentActionOption, 0)
	options, _ := s.config.GetOptions()
	for name := range options {
		optionsList = append(optionsList, slack.AttachmentActionOption{
			Text:        name,
			Description: "gitaly",
			Value:       name,
		})
	}
	return optionsList
}

func (s *Slack) leaveChannel(channel string) {
	log.Println("Leaving channel", channel)
	if ok, err := s.rtm.LeaveChannel(channel); !ok {
		log.Println("Could not leave channel", channel, "due to", err)
	}
}
